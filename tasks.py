from invoke.context import Context

PYTEST_COMMAND_ARGS = "--show-capture=all --log-format='%(levelname)s %(message)s' --cov=ddv.tonab --cov-report term-missing --cov-branch tests/unit"
SRC_DIR = "src"
CONTEXT = Context()


def test_unit():
    with CONTEXT.cd(SRC_DIR):
        CONTEXT.run(
            f"poetry run pytest {PYTEST_COMMAND_ARGS}",
            pty=True,
            warn=True,
        )


def test_manual():
    with CONTEXT.cd(SRC_DIR):
        CONTEXT.run(
            "poetry run python -W ignore tests/manual/manual_test.py",
            pty=True,
            warn=True,
            env={"PYTHONPATH": "."},
        )


def watch():
    with CONTEXT.cd(SRC_DIR):
        CONTEXT.run(
            f"ptw . -- {PYTEST_COMMAND_ARGS}",
            env={"PYTHONPATH": "."},
            pty=True,
        )
