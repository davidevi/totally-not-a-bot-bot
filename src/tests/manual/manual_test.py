import os
import logging
from time import sleep

from ddv.logging import log_to_stdout

from ddv.tonab import Bot, ScreenElement

logger = logging.getLogger(__name__)


def do_countdown():
    COUNT = 3
    for i in range(0, COUNT):
        logger.info(f"{COUNT - i}...")
        sleep(1)
    logger.info("Go")


def main():
    # 2560 x 1600
    # Davidevitelaru.com full screen, brave browser

    log_to_stdout(
        logging_level=logging.DEBUG,
        enable_colours=True,
        enable_indentation=True,
        verbosity_level=0,
        verbosity_filters={1: "PIL"},
    )

    # MENU_BTN_WIDTH = 60
    # MENU_BTN_HEIGHT = 25
    # MENU_BAR_Y = 170

    # home_button = ScreenElement(
    #     name="Home Button",
    #     x=985,
    #     y=MENU_BAR_Y,
    #     width=MENU_BTN_WIDTH,
    #     height=MENU_BTN_HEIGHT,
    # )
    # software_button = ScreenElement(
    #     name="Software Button",
    #     x=1061,
    #     y=MENU_BAR_Y,
    #     width=MENU_BTN_WIDTH + 10,
    #     height=MENU_BTN_HEIGHT,
    # )
    home_button = ScreenElement(
        name="Home Button",
        image_path=f"{os.path.dirname(__file__)}/data/home_cropped.png",
    )
    software_button = ScreenElement(
        name="Software Button",
        image_path=f"{os.path.dirname(__file__)}/data/software_cropped.png",
    )
    tutorials_button = ScreenElement(
        name="Tutorials Button",
        image_path=f"{os.path.dirname(__file__)}/data/tutorials_cropped.png",
    )
    aboutme_button = ScreenElement(
        name="About Me Button",
        image_path=f"{os.path.dirname(__file__)}/data/aboutme_cropped.png",
    )
    newtab_button = ScreenElement(
        name="New Tab Button",
        image_path=f"{os.path.dirname(__file__)}/data/new_tab_cropped.png",
    )
    davetab = ScreenElement(
        name="Dave Tab",
        image_path=f"{os.path.dirname(__file__)}/data/dave_tab.png",
    )

    do_countdown()

    bot = Bot()
    PAUSE = 0.1

    bot.click_element(home_button)
    sleep(PAUSE)
    bot.click_element(software_button)
    sleep(PAUSE)
    bot.click_element(tutorials_button)
    sleep(PAUSE)
    bot.click_element(aboutme_button)
    sleep(PAUSE)
    bot.click_element(newtab_button)
    sleep(PAUSE)
    bot.type_text("The quick brown fox jumps over the lazy dog")
    sleep(PAUSE)
    bot.send_keys("enter")
    sleep(PAUSE)
    bot.click_element(davetab)
    sleep(PAUSE)
    bot.click_element(tutorials_button)
    sleep(PAUSE)
    bot.click_element(aboutme_button)


if __name__ == "__main__":
    main()
