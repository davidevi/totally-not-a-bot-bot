from unittest import TestCase
from unittest import mock

import pyautogui

from ddv.tonab.screen_element import ScreenElement
from ddv.tonab.cursor_controller import (
    CursorController,
    get_fibonacci_list,
    fibonacci_list_to_percentages,
)


class TestCursorController(TestCase):
    def test_init(self):
        CursorController()

    @mock.patch("ddv.tonab.cursor_controller.pyautogui.position")
    def test_distance_to_element(self, mock_cursor_position):
        cc = CursorController()

        mock_cursor_position.return_value = pyautogui.Point(0, 400)
        element = ScreenElement(name="Test Element", x=0, y=0, width=0, height=0)
        distance = cc._distance_to_element(element)
        self.assertEqual(distance, 400)

        mock_cursor_position.return_value = pyautogui.Point(400, 0)
        element = ScreenElement(name="Test Element", x=0, y=0, width=0, height=0)
        distance = cc._distance_to_element(element)
        self.assertEqual(distance, 400)

        mock_cursor_position.return_value = pyautogui.Point(400, 400)
        element = ScreenElement(name="Test Element", x=0, y=0, width=0, height=0)
        distance = cc._distance_to_element(element)
        self.assertEqual(distance, 566)

        mock_cursor_position.return_value = pyautogui.Point(0, 0)
        element = ScreenElement(name="Test Element", x=400, y=400, width=0, height=0)
        distance = cc._distance_to_element(element)
        self.assertEqual(distance, 566)

        mock_cursor_position.return_value = pyautogui.Point(0, 400)
        element = ScreenElement(name="Test Element", x=400, y=0, width=0, height=0)
        distance = cc._distance_to_element(element)
        self.assertEqual(distance, 566)

    @mock.patch("ddv.tonab.config.feature_simulate_touchpad_enabled", True)
    @mock.patch("ddv.tonab.config.feature_simulate_touchpad_minimum_strokes", 1)
    def test_generate_strokes_standard(self):

        cc = CursorController()
        cc.MAX_STROKE_DISTANCE = 400
        strokes = cc._generate_strokes(800)
        self.assertEqual(2, len(strokes))
        self.assertEqual(0, strokes[0]["number"])
        self.assertEqual(800 * 0.67, strokes[0]["distance"])
        self.assertEqual(1, strokes[1]["number"])
        self.assertEqual(800 * 0.33, strokes[1]["distance"])

        cc.MAX_STROKE_DISTANCE = 200
        strokes = cc._generate_strokes(800)
        self.assertEqual(4, len(strokes))
        self.assertEqual(0, strokes[0]["number"])
        self.assertEqual(800 * 0.45, strokes[0]["distance"])
        self.assertEqual(1, strokes[1]["number"])
        self.assertEqual(800 * 0.27, strokes[1]["distance"])
        self.assertEqual(2, strokes[2]["number"])
        self.assertEqual(800 * 0.18, strokes[2]["distance"])
        self.assertEqual(3, strokes[3]["number"])
        self.assertEqual(800 * 0.09, strokes[3]["distance"])

    @mock.patch("ddv.tonab.config.feature_simulate_touchpad_enabled", True)
    @mock.patch("ddv.tonab.config.feature_simulate_touchpad_minimum_strokes", 4)
    def test_generate_strokes_force_more_strokes(self):
        cc = CursorController()
        cc.MAX_STROKE_DISTANCE = 800
        strokes = cc._generate_strokes(800)
        self.assertEqual(4, len(strokes))
        self.assertEqual(0, strokes[0]["number"])
        self.assertEqual(800 * 0.45, strokes[0]["distance"])
        self.assertEqual(1, strokes[1]["number"])
        self.assertEqual(800 * 0.27, strokes[1]["distance"])
        self.assertEqual(2, strokes[2]["number"])
        self.assertEqual(800 * 0.18, strokes[2]["distance"])
        self.assertEqual(3, strokes[3]["number"])
        self.assertEqual(800 * 0.09, strokes[3]["distance"])


class TestFibonacci(TestCase):
    def test_get_fibonacci_list(self):

        sequence = {
            1: [1],
            2: [1, 2],
            3: [1, 2, 3],
            4: [1, 2, 3, 5],
            5: [1, 2, 3, 5, 8],
            6: [1, 2, 3, 5, 8, 13],
            7: [1, 2, 3, 5, 8, 13, 21],
        }

        for key in sequence:
            result = get_fibonacci_list(key)
            self.assertListEqual(sequence[key], result)

    def test_fibonacci_list_to_percentages(self):

        test_cases = [
            {
                "sequence": [1],
                "percentages": [1],
            },
            {
                "sequence": [1, 2],
                "percentages": [0.33, 0.67],
            },
            {
                "sequence": [1, 2, 3],
                "percentages": [0.17, 0.33, 0.5],
            },
            {
                "sequence": [1, 2, 3, 5],
                "percentages": [0.09, 0.18, 0.27, 0.45],
            },
            {
                "sequence": [1, 2, 3, 5, 8],
                "percentages": [0.05, 0.11, 0.16, 0.26, 0.42],
            },
            {
                "sequence": [1, 2, 3, 5, 8, 13],
                "percentages": [0.03, 0.06, 0.09, 0.16, 0.25, 0.41],
            },
            {
                "sequence": [1, 2, 3, 5, 8, 13, 21],
                "percentages": [0.02, 0.04, 0.06, 0.09, 0.15, 0.25, 0.4],
            },
        ]

        for test_case in test_cases:
            result = fibonacci_list_to_percentages(test_case["sequence"])
            self.assertListEqual(result, test_case["percentages"])
