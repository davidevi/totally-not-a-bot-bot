# Words per minute when typing
keyboard_words_per_minute = 82  # 82
# Odds of a mistake when typing
keyboard_mistake_odds = 1 / 30

# UI scaling factor, helpful when coordinates of items are identified
# incorrectly on the screen because of high-dpi screens
ui_scaling = 0.5

# Cursor speed measured in number of pixels traversed per each 100ms
cursor_speed_pixels_per_100ms = 300

# Enable touch pad simulation feature
feature_simulate_touchpad_enabled = True
# How many strokes should take to move the cursor from one corner
# of the screen to the opposite corner
feature_simulate_touchpad_maximum_strokes = 5
# Minimum number of strokes that should be taken to move the cursor
feature_simulate_touchpad_minimum_strokes = 2
# Time to wait between strokes in seconds
feature_simulate_touchpad_100ms_between_strokes = 2
# Adds shakiness to the mouse movements
feature_simulate_touchpad_movement_shake_pixels = 50

# Enable imprecise clicking feature
# Makes cursor click *somewhere* on an element rather than directly
# in its centre
feature_imprecise_clicking_enable = True

# Enable overshooting feature
feature_overshooting_pixels = 200
