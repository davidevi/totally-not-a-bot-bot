import logging

import pyautogui

from ddv.tonab.screen_element import ScreenElement
from ddv.tonab.cursor_controller import CursorController
from ddv.tonab.keyboard_controller import KeyboardController


class Bot(object):

    logger = logging.getLogger(__name__ + ".Bot")

    def __init__(self):
        self.logger.info("Bot initialised")
        self.cursor_controller = CursorController()
        self.keyboard_controller = KeyboardController()

    def click_element(self, element: ScreenElement):
        element.find_coordinates_from_image()
        self.cursor_controller.move_cursor_to_element(element)
        self.logger.info(f"Clicking element {element}")
        pyautogui.click()

    def type_text(self, text):
        self.logger.debug(f"Sending text '{text}'")
        self.keyboard_controller.send_text(text)

    def send_keys(self, keys):
        self.logger.debug(f"Sending keys '{keys}'")
        self.keyboard_controller.send_special_keys(keys)
