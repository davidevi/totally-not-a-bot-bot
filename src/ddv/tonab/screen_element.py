import os
import time
import logging

import pyautogui
from ddv.tonab import config


class ScreenElement(object):

    logger = logging.getLogger(__name__ + ".ScreenElement")

    def __init__(
        self,
        name: str,
        x: int = None,
        y: int = None,
        width: int = None,
        height: int = None,
        image_path: str = None,
    ):
        self.name = name
        self.set_coordinates(x, y, width, height)

        if image_path is not None:
            self.logger.debug(
                f"Creating element '{name}' from image (Path: '{image_path}')"
            )
            self.image_path = image_path

            if not os.path.exists(self.image_path):
                self.logger.error(f"File not found: {self.image_path}")
                raise IOError(f"File not found: {self.image_path}")

        elif x is not None or y is not None or width is not None or height is not None:
            self.logger.debug(
                f"Creating element '{name}' from coordinates ({x}, {y}, {width}, {height})"
            )
            # Already created from self.set_coordinates()
            # Method has to be called early in order to actually set the
            # self.x/y/width/height attributes
        else:
            self.logger.error(
                "Cannot create element without coordinates nor path to image"
            )
            raise Exception(
                "Cannot create element without coordinates nor path to image"
            )

    def __str__(self):
        return f"{self.name} ({self.x}, {self.y}, {self.width}, {self.height})"

    @property
    def found(self):
        return (
            self.x is not None
            or self.y is not None
            or self.width is not None
            or self.height is not None
        )

    @property
    def centre(self):
        centre_x = self.x + self.width / 2
        centre_y = self.y + self.height / 2
        return (centre_x, centre_y)

    def set_coordinates(self, x, y, width, height):
        self.logger.debug(
            f"Setting coordinates for element {self.name}: {x}, {y}, {width}, {height}"
        )
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def find_coordinates_from_image(self):
        if not self.found:
            self.logger.debug(f"Finding element '{self.name}'")
            start_time = time.time()
            box = pyautogui.locateOnScreen(self.image_path)
            if box is not None:
                self.set_coordinates(
                    x=box.left * config.ui_scaling,
                    y=box.top * config.ui_scaling,
                    width=box.width * config.ui_scaling,
                    height=box.height * config.ui_scaling,
                )
            else:
                message = f"Could not find locate element '{self.name}'"
                self.logger.error(message)
                raise Exception(message)
            elapsed_time = time.time() - start_time
            self.logger.debug(
                f"Element '{self.name}' found in {elapsed_time:.2f} seconds"
            )
