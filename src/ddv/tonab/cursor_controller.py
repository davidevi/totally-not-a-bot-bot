import math
import logging
import time
from random import randint

import pyautogui

from ddv.tonab.screen_element import ScreenElement
from ddv.tonab import config


class CursorController(object):

    logger = logging.getLogger(__name__ + ".CursorController")

    def __init__(self):
        self.logger.debug("Initialising Cursor Controller")

        if config.feature_simulate_touchpad_enabled:
            self.MAX_STROKE_DISTANCE = self._calculate_max_cursor_stroke_distance()
            self.logger.debug(
                f"Maximum stroke distance calculated as {self.MAX_STROKE_DISTANCE} pixels"
            )

    def move_cursor_to_element(self, element):

        element.find_coordinates_from_image()

        distance_to_element = self._distance_to_element(element)
        self.logger.debug(
            f"Element '{element}' is {distance_to_element} pixels away from cursor"
        )

        if not config.feature_simulate_touchpad_enabled:
            target_y = element.y + element.height / 2
            target_x = element.x + element.width / 2
            pyautogui.moveTo(target_x, target_y, duration=0.25)
            return

        strokes = self._generate_strokes(distance_to_element)
        self._perform_strokes(strokes, element, distance_to_element)

    def _generate_strokes(self, distance_to_element):
        """Generates a sequence of strokes for simulating moving
        the cursor using a touchpad
        """
        self.logger.debug(
            f"Generating strokes for element {distance_to_element} pixels away"
        )

        # Calculate how many strokes the movement should be made in
        number_of_strokes = round(distance_to_element / self.MAX_STROKE_DISTANCE, 0)
        if number_of_strokes < config.feature_simulate_touchpad_minimum_strokes:
            number_of_strokes = config.feature_simulate_touchpad_minimum_strokes
        self.logger.debug(f"Movement will be made in {number_of_strokes} strokes")

        # Divide up the strokes into pixels
        # - First few strokes should be big and at normal speed
        # - Last 1-2 strokes should be a bit slower
        strokes = []
        number = 0
        stroke_distances_spread = fibonacci_list_to_percentages(
            get_fibonacci_list(number_of_strokes)
        )

        # `reversed()` because we start with long strokes and end with short ones
        for stroke_distance in reversed(stroke_distances_spread):
            self.logger.debug(
                f"Stroke distance spread for stroke #{number} is {stroke_distance}"
            )
            stroke_options = {
                "number": number,
                "distance": distance_to_element * stroke_distance,
            }
            self.logger.debug(
                f"Stroke #{number} created with distance {stroke_options['distance']}"
            )
            strokes.append(stroke_options)
            number += 1

        return strokes

    def _perform_strokes(self, strokes, element, distance_to_element):
        """
        Given a list of strokes, performs the cursor movement in
        accordance with the stroke parameters
        """
        # Shortening variable name
        traversed = 0

        for stroke in strokes:
            random_wait_time = randint(
                int(config.feature_simulate_touchpad_100ms_between_strokes / 2),
                int(config.feature_simulate_touchpad_100ms_between_strokes * 2),
            )
            wait_time_ms = random_wait_time / 10.0
            self.logger.debug(f"Waiting {wait_time_ms} seconds between strokes")
            time.sleep(wait_time_ms)

            self.logger.debug(
                f"Performing stroke #{stroke['number'] + 1}/{len(strokes)}"
            )
            if stroke["number"] + 1 == len(strokes):
                if config.feature_imprecise_clicking_enable:
                    target_x = randint(int(element.x), int(element.x + element.width))
                    target_y = randint(int(element.y), int(element.y + element.height))
                else:
                    (target_x, target_y) = element.centre
            else:
                cursor_position = pyautogui.position()
                xa = cursor_position.x
                ya = cursor_position.y
                xb = element.x
                yb = element.y

                # https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
                d2 = stroke["distance"]
                d = distance_to_element

                x_shake = randint(
                    -config.feature_simulate_touchpad_movement_shake_pixels,
                    config.feature_simulate_touchpad_movement_shake_pixels,
                )
                y_shake = randint(
                    -config.feature_simulate_touchpad_movement_shake_pixels,
                    config.feature_simulate_touchpad_movement_shake_pixels,
                )
                self.logger.debug(
                    f"Adding mouse shake of {x_shake} (X) and {y_shake} (Y)"
                )
                target_x = xb - ((d2 * (xb - xa)) / d) + x_shake
                target_y = yb - ((d2 * (yb - ya)) / d) + y_shake

            duration_100ms = distance_to_element / config.cursor_speed_pixels_per_100ms

            traversed += stroke["distance"]
            self.logger.debug(
                f"Moving {round(stroke['distance'])} pixels to {round(target_x)} x {round(target_y)} | Traversed {round(traversed,0)}/{distance_to_element} pixels"
            )
            pyautogui.moveTo(target_x, target_y, duration=duration_100ms / 10)

    def _distance_to_element(self, element: ScreenElement) -> int:
        """
        Returns the distance in pixels from the cursor to a random spot close
        to the element's centre
        """
        cursor_position = pyautogui.position()
        cursor_x_scaled = cursor_position.x
        cursor_y_scaled = cursor_position.y
        (x, y) = element.centre

        overshoot = randint(
            int(config.feature_overshooting_pixels / 4),
            int(config.feature_overshooting_pixels * 2),
        )
        self.logger.debug(
            f"Adding {overshoot} pixels of overshoot to distance to '{element.name}'"
        )
        return (
            round(math.hypot(cursor_x_scaled - x, cursor_y_scaled - y), 2) + overshoot
        )

    def _calculate_max_cursor_stroke_distance(self):
        """Cursor should only move by a fourth of the diagonal at once
        in order to simulate a cursor moved on a touchpad
        """

        # Calculating length of diagonal
        screen_size = pyautogui.size()
        self.logger.debug(
            f"Screen size determined as {screen_size.width}x{screen_size.height}"
        )
        diagonal_size = math.hypot(screen_size.width, screen_size.height)
        self.logger.debug(f"Diagonal length determined as {diagonal_size} pixels")

        # Maximum stroke size should be a third of the diagonal
        return round(diagonal_size / 3.0)


def get_fibonacci_list(nterms):
    """
    Returns `nterms` items of the Fibonacci sequence starting from the 3rd element (i.e.
    ignoring the first 0 and 1) for `nterms`
    """

    n1, n2 = 0, 1
    count = 0

    if nterms <= 0:
        raise Exception("Must be ")

    if nterms == 1:
        return [1]

    to_return = []
    while count < nterms + 2:
        to_return.append(n1)
        nth = n1 + n2
        # update values
        n1 = n2
        n2 = nth
        count += 1
    return to_return[2:]


def fibonacci_list_to_percentages(sequence_list):
    """
    Given a fibonnaci sequence, turns each item into a percenteage of the
    overall sum of all items in the sequence
    """
    pct_sequence = []

    total = sum(sequence_list)

    for item in sequence_list:
        pct_sequence.append(round(item / float(total), 2))

    return pct_sequence
