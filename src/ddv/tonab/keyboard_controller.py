import string
import time
import random
import logging

import pyautogui

from ddv.tonab import config


class KeyboardController(object):

    logger = logging.getLogger(__name__ + ".KeyboardController")

    def __init__(self):
        self.logger.debug("Initialising Keyboard Controller")

        characters_per_minute = config.keyboard_words_per_minute * 5
        self.characters_per_second = round(characters_per_minute / 60.0, 2)

    def send_text(self, text):
        self.logger.debug(f"Typing text '{text}'")
        typed_characters = 0
        start = time.time()
        for character in text:

            mistake_roll = random.randint(0, 100) / 100
            if mistake_roll < config.keyboard_mistake_odds:
                self.logger.debug(
                    f"Mistake made with {mistake_roll} / {config.keyboard_mistake_odds} "
                )
                pyautogui.write(random.choice(string.ascii_lowercase))
                random_wait = random.randint(50, 200)
                time.sleep(random_wait / 1000)
                self.send_special_keys("backspace")
                random_wait = random.randint(50, 200)
                time.sleep(random_wait / 1000)

            pyautogui.write(character)
            typed_characters += 1

            # Longer breaks after space
            if character == " ":
                random_wait = random.randint(10, 60)
            else:
                random_wait = random.randint(0, 50)

            self.logger.debug(f"Waiting for {random_wait} milliseconds")
            time.sleep(random_wait / 1000)

            # Adjusting WPM in case the typing is too fast
            if typed_characters >= self.characters_per_second:
                self.logger.debug(
                    f"Typed {typed_characters} / {self.characters_per_second}"
                )
                typed_characters = 0
                end = time.time()
                if end - start < 1:
                    self.logger.debug(
                        f"Adjusting WPM; Waiting for { 1 - end + start} seconds"
                    )
                    time.sleep(1 - end + start)
                start = time.time()

    def send_special_keys(self, special):
        pyautogui.press(special)

        # TODO: Allow for key combos
